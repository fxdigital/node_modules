const confusingBrowserGlobals = require("confusing-browser-globals");

const globals = ["error", "isFinite", "isNaN"].concat(confusingBrowserGlobals);

module.exports = (allow = []) => {
  return globals.filter(value => !allow.includes(value));
};
