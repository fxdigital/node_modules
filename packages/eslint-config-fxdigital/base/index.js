const restrictedGlobals = require("../util/restricted-globals");

module.exports = {
  extends: ["airbnb-base", "prettier"],
  globals: {
    __DEV__: true
  },
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "error",
    "no-restricted-globals": restrictedGlobals()
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: [".mjs", ".js", ".json"]
      }
    },
    "import/extensions": [".js", ".mjs"],
    "import/core-modules": [],
    "import/ignore": [
      "node_modules",
      "\\.(coffee|sass|scss|css|less|hbs|svg|json)$"
    ]
  }
};
