const { resolve } = require("path");

module.exports = {
  extends: "@fxdigital/eslint-config-fxdigital",
  settings: {
    "import/resolver": {
      "eslint-import-resolver-lerna": {
        packages: resolve(__dirname, "packages")
      }
    }
  }
};
