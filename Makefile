nbin = ./node_modules/.bin
lerna = $(nbin)/lerna
lrun = $(lerna) run
eslint = $(nbin)/eslint . --ext .js,.jsx --format=codeframe
prettier = $(nbin)/prettier "./**/*.{css,sass,scss,js,json,jsx,md,mjs,yml,yaml}"

build:
	$(lrun) build --stream

commit:
	 $(nbin)/git-cz

format:
	$(prettier) --write
	$(eslint) --fix

lint:
	$(prettier) --check
	$(eslint)

release:
	 $(lerna) version --exact

test:
	$(lrun) test

travis-lint:
	$(prettier) --check || TEST_EC=$${?}; \
	$(eslint) || TEST_EC=$${?}; \
	exit $${TEST_EC}
