module.exports = {
  extends: [
    "../browser/index.js",
    "eslint-config-airbnb-base/rules/strict.js",
    require.resolve("eslint-config-airbnb/rules/react"),
    require.resolve("eslint-config-airbnb/rules/react-a11y"),
    "plugin:react-perf/all",
    "prettier",
    "prettier/react"
  ],
  plugins: ["react-perf"],
  globals: {
    window: true,
    document: true
  },
  settings: {
    "import/resolver": {
      node: {
        extensions: [".jsx", ".mjs", ".js", ".json"]
      }
    },
    "import/extensions": [".jsx", ".js", ".mjs"]
  }
};
