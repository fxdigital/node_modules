#!/usr/bin/env bash
set -e

CONFIGS=(
    'css'
    'scss'
)

for CONFIG in ${CONFIGS[@]} ; do
    echo "------> Testing \`./${CONFIG}/index.js\`"
    ./node_modules/.bin/stylelint-config-prettier-check "./${CONFIG}/.stylelintrc.json"
done
