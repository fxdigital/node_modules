#!/usr/bin/env bash

CONFIGS=(
    'base'
    'browser'
    'node'
    'react'
    'worker'
)

for CONFIG in ${CONFIGS[@]} ; do
    echo "------> Testing \`./${CONFIG}/index.js\`"
    ./node_modules/.bin/eslint -c "./${CONFIG}/index.js" --print-config . | ./node_modules/.bin/eslint-config-prettier-check
done
