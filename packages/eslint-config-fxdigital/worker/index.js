const restrictedGlobals = require("../util/restricted-globals");

module.exports = {
  extends: ["../base/index.js"],
  globals: {
    window: true,
    self: true
  },
  rules: {
    "no-restricted-globals": restrictedGlobals(["self"])
  }
};
