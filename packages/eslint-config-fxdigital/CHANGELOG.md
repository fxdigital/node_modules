# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.0.2](https://bitbucket.org/fxdigital/node_modules/src/master/packages/eslint-config-fxdigital/compare/@fxdigital/eslint-config-fxdigital@0.0.1...@fxdigital/eslint-config-fxdigital@0.0.2) (2019-10-17)

**Note:** Version bump only for package @fxdigital/eslint-config-fxdigital





## 0.0.1 (2019-03-04)

**Note:** Version bump only for package @fxdigital/eslint-config-fxdigital
